---
created: 2020-12-09T01:40:25+02:00
modified: 2020-12-21T21:30:09+02:00
---

# Typescript Handbook

# Simple types
## boolean
## number
## bigint
## array
## tuple
## unknown
## any
## void
## null
when using the --strictNullChecks flag, null and undefined are only assignable to unknown
## undefined
## never
## object
# type assertions
## as-syntax
## angle brackets
# Interfaces
## optional properties
## readonly properties
### ReadOnlyArray<>
## Excess Property Checks
## Function Types
interface SearchFunc {
  (source: string, subString: string): boolean;
}
## Indexable Types
interface StringArray {
  [index: number]: string;
}
## Class Types
 Clock implements ClockInterface
### static side of classes
interface ClockConstructor {
  new (hour: number, minute: number): ClockInterface;
}
function createClock(
  ctor: ClockConstructor,
  hour: number,
  minute: number
): ClockInterface {
  return new ctor(hour, minute);
}
let digital = createClock(DigitalClock, 12, 17);
### class expression
const Clock: ClockConstructor = class Clock implements ClockInterface {
  constructor(h: number, m: number) {}
  tick() {
    console.log("beep beep");
  }
};
## Hybrid types
interface Counter {
  (start: number): string;
  interval: number;
  reset(): void;
}
## Interfaces Extending Classes
when you create an interface that extends a class with private or protected members, that interface type can only be implemented by that class or a subclass of it.
# Functions
## type
let myAdd: (x: number, y: number) => number = function (x: number, y: number): number {
  return x + y;
};
## optional parameter
function buildName(firstName: string, lastName?: string) {
## default parameters
function buildName(firstName: string, lastName = "Smith") {
## rest parameters
function buildName(firstName: string, ...restOfName: string[]) {
##  --noImplicitThis
## Overloads
# Literal Types
## Literal Narrowing
const helloWorld:"Hello World" = "Hello World";
# Unions and Intersection Types
## Unions with Common Fields
## Discriminating Unions
type NetworkLoadingState = {
  state: "loading";
};

type NetworkFailedState = {
  state: "failed";
  code: number;
};

type NetworkSuccessState = {
  state: "success";
  response: {
    title: string;
    duration: number;
    summary: string;
  };
};
## Union Exhaustiveness checking
  switch (s.state) {
    case "loading":
      return "loading request";
    case "failed":
      return `failed with code ${s.code}`;
    case "success":
      return "got response";
    default:
      return assertNever(s);
}
# Classes
## Public, private, and protected modifiers
### Public by default
### ECMAScript Private Fields
### TypeScript’s private
For two types to be considered compatible, if one of them has a private member, then the other must have a private member that originated in the same declaration. The same applies to protected members.
### Readonly modifier
### Parameter properties
constructor(readonly name: string) {}
### Accessors
  get fullName(): string {
    return this._fullName;
  }
### Static Properties
### Abstract Classes
## Advanced Techniques
### Constructor functions
class Greeter {...
let greeterMaker: typeof Greeter = Greeter;
greeterMaker.standardGreeting = "Hey there!";

# Enums
## Computed and constant members
enum FileAccess {
  // constant members
  None,
  Read = 1 << 1,
  Write = 1 << 2,
  ReadWrite = Read | Write,
  // computed member
  G = "123".length,
}
## Union enums and enum member types
## Enums at runtime
enum E {
  X,
  Y,
  Z,
}

function f(obj: { X: number }) {
  return obj.X;
}

// Works, since 'E' has a property named 'X' which is a number.
f(E);
## Enums at compile time
### typeof
type LogLevelStrings = keyof typeof LogLevel;
### Reverse mappings
let a = Enum.A;
let nameOfA = Enum[a]; 
### const enums
Const enums can only use constant enum expressions and unlike regular enums they are completely removed during compilation.
 ## Ambient enums.
# Generics
function identity<T>(arg: T): T {
## Generic Types (interfaces)
## Generic Classes
## Generic Constraints
## Using Type Parameters in Generic Constraints
function getProperty<T, K extends keyof T>(obj: T, key: K) {
  return obj[key];
}
## Using Class Types in Generics
function create<T>(c: { new (): T }): T {
  return new c();
}